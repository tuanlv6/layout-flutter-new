import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app10/data_bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class National extends StatefulWidget {
  National({Key? key}) : super(key: key);

  @override
  _NationalState createState() => _NationalState();
}

class _NationalState extends State<National> {
  Color _favIconColor = Colors.grey;
  bool _favorite = false;

  @override
  Widget build(BuildContext context) {
    final DataBloc favoriteBloc = BlocProvider.of<DataBloc>(context);
    return Scaffold(
      appBar: AppBar(),
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: 500,
        margin: const EdgeInsets.all(15.0),
        padding: const EdgeInsets.all(3.0),
        decoration: BoxDecoration(border: Border.all(color: Colors.blueAccent),borderRadius: BorderRadius.circular(15.0)),
        child: Column(
          children: <Widget>[
            Stack(
              children: <Widget>[
                Container(
                  child: ClipRRect(
                    borderRadius: BorderRadius.only(topLeft: Radius.circular(15.0),topRight: Radius.circular(15.0)),
                    child: Image(
                      image: AssetImage("assets/backgroundnew.jpg"),
                      fit: BoxFit.fill,
                    ),
                  ),
                  width: MediaQuery.of(context).size.width,
                  height: 200,
                ),
                BlocBuilder<DataBloc,DataState>(builder:(context,state){
                  if(state is Favorite){
                    return Container(
                      child: IconButton(
                        icon: Icon(Icons.favorite,size: 30,),
                        color: state.favorite? Colors.deepPurple:Colors.grey,
                        tooltip: 'Add to favorite',
                        onPressed: () {
                          favoriteBloc.add(ChangeFavorite());
                          // setState(() {
                          //   _favorite = !_favorite;
                          //   // if (_favIconColor == Colors.grey) {
                          //   //   _favIconColor = Colors.deepPurple;
                          //   // } else {
                          //   //   _favIconColor = Colors.grey;
                          //   // }
                          // });
                        },
                      ),
                      margin: EdgeInsets.only(top: 20, left: 40),
                      width: 50,
                      height: 50,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          border: Border.all(
                            color: Colors.white,
                          ),
                          borderRadius: BorderRadius.all(Radius.circular(15))),
                    );
                  } else {
                    return new CircularProgressIndicator();
                  }
                })
              ],
            ),
            Container(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                      margin: EdgeInsets.only(left: 10, top: 20),
                      child: Text(
                        'EVENTS',
                        style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                            color: Colors.deepPurple),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                      margin: EdgeInsets.only(left: 10),
                      child: Text(
                        'National health movement',
                        style: TextStyle(
                            fontSize: 25,
                            fontWeight: FontWeight.bold,
                            color: Colors.black),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                      margin: EdgeInsets.only(left: 10, top: 2.0),
                      child: Container(
                        width: 300,
                        child: Text(
                          'Challenge your friends by talking the most steps on Saturday and Sunday! ',
                          style: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.bold,
                              color: Colors.blueGrey),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Container(
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(8.0),
                          child: Image(
                            image: AssetImage("assets/picture.jpg"),
                            fit: BoxFit.fill,
                          ),
                        ),
                        height: 50,
                        width: 50,
                        margin: EdgeInsets.only(left: 10, top: 20),
                      ),
                    ),
                    Container(
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(8.0),
                        child: Image(
                          image: AssetImage("assets/picture.jpg"),
                          fit: BoxFit.fill,
                        ),
                      ),
                      height: 50,
                      width: 50,
                      margin: EdgeInsets.only(left: 5, top: 20),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Container(
                        margin: EdgeInsets.only(left : 5 ,top: 22),
                        width: 50,
                        height: 50,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Colors.blue,
                          border: Border(
                            left: BorderSide(
                              color: Colors.blue,
                              width: 3,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        children: <Widget>[
                          Container(
                            child: Center(
                                child: Text('Join',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.purple,
                                    ))),
                            margin: EdgeInsets.only(right: 15, top: 20),
                            width: 120,
                            height: 50,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              color: Colors.purple[50],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
