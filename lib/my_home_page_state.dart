import 'package:flutter/material.dart';

import 'my_stream.dart';

class MyHomePageState extends StatefulWidget {
  MyHomePageState({Key? key}) : super(key: key);

  @override
  _MyHomePageStateState createState() => _MyHomePageStateState();
}

class _MyHomePageStateState extends State<MyHomePageState> {
  MyStream myStream = new MyStream();

  @override
  void dispose() {
    myStream.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    myStream.counterStream.listen((event) {
      print(event.toString());
    });

    return Scaffold(
      appBar: AppBar(
        title: Text('Broadcast Streams'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'You have pushed the button this many times:',
            ),
            StreamBuilder(
              stream: myStream.counterStream,
              builder: (context, snapshot) =>
                  Text(
                    snapshot.hasData ? snapshot.data.toString() : "0",
                    style: Theme
                        .of(context)
                        .textTheme
                        .headline4,
                  ),
            )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          myStream.increment();
        },
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ),
    );
  }
}