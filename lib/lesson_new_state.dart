part of 'lesson_new_bloc.dart';

@immutable
abstract class LessonNewState {}

class LessonNewInitial extends LessonNewState {}

class Loading extends LessonNewState {}

class Success extends LessonNewState {
  int count = 0;

  Success(this.count);

}