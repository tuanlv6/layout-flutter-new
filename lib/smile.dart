import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Smile extends StatefulWidget {
  Smile({Key? key}) : super(key: key);

  @override
  _SmileState createState() => _SmileState();
}

class _SmileState extends State<Smile> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Container(
        margin: const EdgeInsets.all(15.0),
        padding: const EdgeInsets.all(3.0),
        decoration: BoxDecoration(border: Border.all(color: Colors.blueAccent)),
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Container(
          padding: EdgeInsets.all(10.0),
          margin: EdgeInsets.only(top: 10.0, bottom: 10.0, left: 20.0),
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text('Get Inspired',
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
                  Container(margin: EdgeInsets.only(), child: Icon(Icons.add))
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: 10),
                        width: 60,
                        height: 60,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: Colors.yellow),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 10, left: 20),
                        padding:
                        EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
                        width: 210,
                        height: 60,
                        color: Colors.red,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text('7 Nutrition Tips To Lose Weight Faster'),
                            Text(
                              'Nutrition',
                              style: TextStyle(color: Colors.yellow),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),

                  Container(

                    child: Icon(Icons.add),
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget> [
                  Row(
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: 10),
                        width: 60,
                        height: 60,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: Colors.yellow),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 10, left: 20),
                        padding:
                            EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
                        width: 210,
                        height: 60,
                        color: Colors.red,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text('7 Nutrition Tips To Lose Weight Faster'),
                            Text(
                              'Nutrition',
                              style: TextStyle(color: Colors.yellow),
                            )
                          ],
                        ),
                      ),

                    ],
                  ),

                  Container(

                    child: Icon(Icons.add),
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget> [
                  Row(
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: 10),
                        width: 60,
                        height: 60,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: Colors.yellow),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 10, left: 20),
                        padding:
                        EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
                        width: 210,
                        height: 60,
                        color: Colors.red,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text('7 Nutrition Tips To Lose Weight Faster'),
                            Text(
                              'Nutrition',
                              style: TextStyle(color: Colors.yellow),
                            )
                          ],
                        ),
                      ),

                    ],
                  ),

                  Container(

                    child: Icon(Icons.add),
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Container(
                    margin: EdgeInsets.only(left: 10, top: 10),
                    child: Text('Collection',
                        style: TextStyle(
                            fontWeight: FontWeight.bold, color: Colors.black)),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    width: 150,
                    height: 150,
                    margin: EdgeInsets.only(top: 10),
                    padding: const EdgeInsets.all(8.0),
                    decoration: BoxDecoration(
                      color: Colors.redAccent,
                      border: Border.all(color: Colors.blueAccent),
                      borderRadius: BorderRadius.circular(15),
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'HEALTH',
                          style: TextStyle(color: Colors.white),
                        ),
                        Container(
                            margin: EdgeInsets.only(left: 20, bottom: 10),
                            child: Text(
                              'How to be Better Runner',
                              style: TextStyle(color: Colors.white),
                            )),
                        Container(
                            child: Text(
                              'B articles',
                              style: TextStyle(color: Colors.deepPurple),
                            ),
                            width: 80,
                            height: 35,
                            margin: EdgeInsets.only(top: 10, bottom: 10),
                            padding: const EdgeInsets.all(8.0),
                            decoration: BoxDecoration(
                              color: Colors.white,
                              border: Border.all(color: Colors.white),
                              borderRadius: BorderRadius.circular(15),
                            )),
                      ],
                    ),
                  ),
                  Container(
                    width: 150,
                    height: 150,
                    margin: EdgeInsets.only(top: 10, left: 10),
                    padding: const EdgeInsets.all(8.0),
                    decoration: BoxDecoration(
                      color: Colors.redAccent,
                      border: Border.all(color: Colors.blueAccent),
                      borderRadius: BorderRadius.circular(15),
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'HEALTH',
                          style: TextStyle(color: Colors.white),
                        ),
                        Container(
                            margin: EdgeInsets.only(left: 20, bottom: 10),
                            child: Text(
                              'How to be Better Runner',
                              style: TextStyle(color: Colors.white),
                            )),
                        Container(
                            child: Text(
                              'B articles',
                              style: TextStyle(color: Colors.deepPurple),
                            ),
                            width: 80,
                            height: 35,
                            margin: EdgeInsets.only(top: 10, bottom: 10),
                            padding: const EdgeInsets.all(8.0),
                            decoration: BoxDecoration(
                              color: Colors.white,
                              border: Border.all(color: Colors.white),
                              borderRadius: BorderRadius.circular(15),
                            )),
                      ],
                    ),
                  )
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
