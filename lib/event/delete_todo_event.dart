import 'package:flutter_app10/base/base_event.dart';
import 'package:flutter_app10/model/todo.dart';

class DeleteTodoEvent extends BaseEvent{

  Todo todo;

  DeleteTodoEvent(this.todo);
}