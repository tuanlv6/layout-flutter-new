import 'package:flutter_app10/base/base_event.dart';

class AddToDoEvent extends BaseEvent{
  String content;

  AddToDoEvent(this.content);

}