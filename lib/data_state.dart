part of 'data_bloc.dart';

@immutable
abstract class DataState {}

class DataInitial extends DataState {}

class Favorite extends DataState{
  bool favorite = true;

  Favorite(this.favorite);
}
