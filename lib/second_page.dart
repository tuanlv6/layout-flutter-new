import 'package:flutter/material.dart';

class SecondPageNew extends StatefulWidget {
  SecondPageNew({Key? key}) : super(key: key);

  @override
  _SecondPageNewState createState() => _SecondPageNewState();
}

class _SecondPageNewState extends State<SecondPageNew> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Text(' Second Page '),
      ),
      body: Center(
        child: ElevatedButton(
           child: Text('Go back to main page'),
           onPressed: (){
             Navigator.pop(context);
           },
        ),
      ),
    );
  }
}