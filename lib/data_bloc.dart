import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'data_event.dart';
part 'data_state.dart';

class DataBloc extends Bloc<DataEvent, DataState> {
  DataBloc() : super(Favorite(true));
  bool _favorite = true ;

  @override
  Stream<DataState> mapEventToState(
    DataEvent event,
  ) async* {
    switch(event.runtimeType){
      case  ChangeFavorite :
        _favorite = !_favorite;
        yield Favorite(_favorite);
        break;
    }
    // TODO: implement mapEventToState
  }
}
