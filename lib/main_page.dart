import 'package:flutter/material.dart';
import 'package:flutter_app10/route_manager.dart';

class MainPage extends StatefulWidget {
  MainPage({Key? key}) : super(key: key);

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  dynamic result = 'Data to come back';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(' Main Page '),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ElevatedButton(
              child: Text('Go to second page'),
              onPressed: () {
                Navigator.of(context).pushNamed(RouteManager.secondPage);
              },
            ),
            SizedBox(
              height: 5,
            ),
            ElevatedButton(
              child: Text('Go to third page'),
              onPressed: () async {
                var resultBack = await Navigator.of(context).pushNamed(RouteManager.thirdPage,arguments: {
                  'name' : 'John Rambo',
                });
                setState(() {
                  result = resultBack;
                });
              },
            ),
            Text('$result'),
          ],
        ),
      ),
    );
  }
}
