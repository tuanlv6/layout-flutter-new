import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';

class Demo extends StatefulWidget {
  Demo({Key? key}) : super(key: key);

  @override
  _DemoState createState() => _DemoState();
}

class _DemoState extends State<Demo> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: SafeArea(
          child: Card(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20)),
              child: Container(
                width: MediaQuery.of(context).size.width,
                height: 450,
                child: Column(
                  children: [
                    Stack(
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                                margin: EdgeInsets.only(bottom: 10,left: 10),
                                child: new Text('BREAKFAST',style: TextStyle(
                                  color: Colors.deepPurple
                                ),)),
                            Container(
                              margin: EdgeInsets.only(left: 5),
                              child: Row(
                                children: [
                                  Icon(Icons.add),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Row(
                                    children: [
                                      Text(
                                        "120",
                                        style: TextStyle(
                                            fontSize: 18,
                                            fontWeight: FontWeight.bold),
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(left: 10),
                                        child: Text(
                                          "kcal/ 450kcal",
                                          style: TextStyle(
                                              fontSize: 14,
                                              fontWeight: FontWeight.normal),
                                        ),
                                      )
                                    ],
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                        Positioned(
                          top: 10,
                          right: 20,
                          child: Container(
                            width: 36,
                            height: 36,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Colors.blue),
                            child: Center(
                              child: Icon(
                                Icons.add,
                                color: Colors.white,
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                    Divider(
                      color: Colors.black,
                      indent: 30,
                      endIndent: 30,
                    ),
                    Row(
                      children: [
                        Container(
                          margin: EdgeInsets.only(left: 10.0),
                          child: Stack(
                            children: [
                              Container(
                                child: Image(image: AssetImage("assets/logo1.jpg"),),
                                color: Colors.yellow,
                                height: 100,
                              ),
                              Positioned(
                                right: 10,
                                bottom: 10,
                                child: Container(
                                  child: Image(image: AssetImage("assets/logo2.jpg"),),
                                  width: 30,
                                  height: 30,
                                  color: Colors.black,
                                  
                                ),
                              )

                            ],
                          ),
                          width:100,
                          color: Colors.blue,
                        ),

                        Expanded(

                          child: Container(
                            margin: EdgeInsets.only(left: 10.0),
                            padding: EdgeInsets.only(top: 10.0,bottom: 10.0,left: 10.0,right: 10.0),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget> [
                                  Text('Salad with wheat  and white egg',style: TextStyle(
                                    fontSize: 25,
                                    fontWeight: FontWeight.bold

                                  ),),
                                  Container(
                                    margin: EdgeInsets.only(top: 10) ,
                                      child: Text('200cals'))
                                ],
                            ),


                            color: Colors.white,

                          ),
                        ),

                      ],
                    ),

                    Row(
                      children: [
                        Container(
                          margin: EdgeInsets.only(left: 10.0),
                          child: Stack(
                            children: [
                              Container(

                                child: Image(image: AssetImage("assets/logo3.jpg")),
                                color: Colors.yellow,
                                height: 80,
                              ),
                              Positioned(
                                right: 10,
                                bottom: 10,
                                child: Container(
                                  child: Image(image: AssetImage("assets/logo4.jpg"),),
                                  width: 30,
                                  height: 30,
                                  color: Colors.black,

                                ),
                              )

                            ],
                          ),
                          width:100,
                          color: Colors.blue,
                        ),

                        Expanded(

                          child: Container(
                            margin: EdgeInsets.only(left: 10.0),
                            padding: EdgeInsets.only(top: 10.0,bottom: 10.0,left: 10.0,right: 10.0),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget> [
                                Text('Pumpkin soup',style: TextStyle(
                                    fontSize: 25,
                                    fontWeight: FontWeight.bold

                                ),),
                                Container(
                                    margin: EdgeInsets.only(top: 10) ,
                                    child: Text('200cals')),
                                Container(
                                    margin: EdgeInsets.only(top: 10) ,
                                    child: Text('VERY HIGH CARB !',style: TextStyle(
                                      color: Colors.deepOrange
                                    ),)),

                              ],
                            ),


                            color: Colors.white,

                          ),
                        ),

                      ],
                    )
                  ],
                ),
              ))),
    );
    ;
  }
}
