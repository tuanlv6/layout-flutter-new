import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'lesson_new_event.dart';
part 'lesson_new_state.dart';

class LessonNewBloc extends Bloc<LessonNewEvent, LessonNewState> {
  LessonNewBloc() : super(LessonNewInitial());

  int count = 0;

  @override
  // TODO: implement initialState
  LessonNewState get initialState => Success(count);
  Stream<LessonNewState> mapEventToState(
    LessonNewEvent event,
  ) async* {
    if (event is Increment) {
      yield Loading();
      await Future.delayed(Duration(seconds: 1));
      count++;
      yield Success(count);
    }
  }
}
