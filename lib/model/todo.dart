import 'package:flutter_app10/todo/todo_list.dart';

class Todo {
  late int _id ;
  late String _content ;

  Todo.fromData(id,content){
    _id = id ;
    _content = _content;
  }

  String get content => _content;

  set content(String value) {
    _content = value;
  }

  int get id => _id;

  set id(int value) {
    _id = value;
  }

  Map<String,dynamic> toMap(){
    return{
      'id': _id,
      'content': _content,

    };
  }
}
