import 'package:flutter/material.dart';
import 'package:flutter_app10/bloc/todo_bloc.dart';
import 'package:flutter_app10/todo/todo_list_container.dart';
import 'package:provider/provider.dart';

import 'db/todo_database.dart';

void main() async {
  await TodoDatabase.instance.init();
  runApp(TodoListMain());
}

class TodoListMain extends StatelessWidget {
  // This widget is the root of your application

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(primarySwatch: Colors.blue),
      home: Scaffold(
          appBar: AppBar(
            title: Text("Todo List "),
          ),
          body: Provider<TodoBloc>.value(
              value :TodoBloc(),
              child: TodoListContainer(),
          ),
      ),
    );
  }
}


