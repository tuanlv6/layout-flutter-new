import 'package:flutter/material.dart';
import 'package:flutter_app10/lesson_new_bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() => runApp(LessonBloc());

class LessonBloc extends StatelessWidget {
  // This widget is the root of your application.
  LessonBloc({Key? key, this.title}) : super(key: key);
  final String? title;
  @override
  Widget build(BuildContext context) {
    final LessonNewBloc lessonBloc = BlocProvider.of<LessonNewBloc>(context);

    return Scaffold(
      appBar: AppBar(
        title: Text('Demo Bloc Flutter'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            BlocBuilder<LessonNewBloc, LessonNewState>(
              builder: (context, state) {
                if (state is Success) {
                  return Text(
                    '${state.count}',
                    style: Theme.of(context).textTheme.headline4,
                  );
                } else {
                  return new CircularProgressIndicator();
                }
              },
            )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          lessonBloc.add(Increment());
        },
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
