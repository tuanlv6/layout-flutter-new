part of 'lesson_new_bloc.dart';

@immutable
abstract class LessonNewEvent {}

class Increment extends LessonNewEvent {}
